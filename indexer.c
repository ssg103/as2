#include "indexer.h"

node *head = NULL;
int size = 0;
void inserter(char *key, char *fname)
{   //printf("%s\n", key);
    int count = 0;
    for(count = 0; count< strlen(fname); count++)
    {
        fname[count] = tolower(fname[count]);
    }

    if(head == NULL)
    {
        //initialize
        head = (node*)malloc(sizeof(node));
        head->value = key;
        head->next = NULL;
        head->fileSize = 1;

        fileNode *fnode = (fileNode*)malloc(sizeof(fileNode));
        fnode->fileName = fname;
        fnode->occurrence = 1;
        fnode->nextNode = NULL;
        head->files = fnode;
        size = 1;
        //printf("%s\t%s\t%d\n", head->value, head->files->fileName, head->files->occurrence);
    }
    else
    {
        //check if key exists
        node* ptr = head;
        char hasFound = '0';
        do
        {
            if(strcmp(ptr->value, key) == 0)
            {
                hasFound = '1';
                break;
            }
            if(ptr->next == NULL)
                break;
            ptr = ptr->next;
        }while(ptr!= NULL);
        if(hasFound == '1')
        {

            //check fileNode to see same file if not then add new fileNode
            fileNode *fn = ptr->files;
            do
            {
                if(strcmp(fn->fileName, fname) == 0)
                {
                    fn->occurrence++;
                    return;
                }
                if(fn->nextNode == NULL)
                    break;
                fn = fn->nextNode;
            }while(fn != NULL);

            //if it hasn't returned then it couldn't find it therefore add new fileNode
            fileNode *newFile = (fileNode*)malloc(sizeof(fileNode));
            newFile->fileName = fname;
            newFile->occurrence = 1;
            newFile->nextNode = NULL;

            fn->nextNode = newFile;
            ptr->fileSize++;


        }
        else
        {
            //add new node

            node* newNode = (node*)malloc(sizeof(node));
            newNode->value = key;
            newNode->next = NULL;
            newNode->fileSize = 1;

            fileNode *fnode = (fileNode*)malloc(sizeof(fileNode));
            fnode->fileName = fname;
            fnode->occurrence = 1;
            fnode->nextNode = NULL;

            newNode->files = fnode;

            node* srt = head;
            node *bup = NULL;
            char hasInserted = '0';
            do
            {
                if(strcmp(srt->value, newNode->value) > 0)
                {
                    //printf("hello");
                    hasInserted = '1';
                    if(bup == NULL)
                    {
                        newNode->next = srt;
                        head = newNode;
                    }
                    else
                    {
                        bup->next = newNode;
                        newNode->next = srt;
                    }

                    break;
                }
                bup = srt;
                srt = srt->next;
            }while(srt!= NULL);
            if(hasInserted  == '0')
            {
                ptr->next = newNode;
            }

            //ptr->next = newNode;
            size++;
        }
    }
}

int checkIfValid(char *fname)
{
    int valid = 1;
    //to make sure that the file is a txt
    char *extension = fname;
    if((extension = strrchr(fname,'.')) != NULL )
    {
        if(strcmp(extension,".txt") != 0)
            valid = 0;

    }
    //
    // if(strcmp(fname, ".")|| fname)
    return valid;
}

void txtReader(const char* filePath, char* dirName)
{
    char *path = NULL;
    if(strcmp(filePath, dirName) != 0)
    {
        int pathSize = strlen(filePath)+1+strlen(dirName)+1;
        path = (char*)malloc(sizeof(char)*pathSize);
        strcpy(path, filePath);
        path[strlen(filePath)] = '/';

        int k = 0;
        int ct = 0;
        for(k = strlen(filePath)+1; k<pathSize-1; k++)
        {
            path[k] = dirName[ct];
            ct++;
        }
        path[k] = '\0';
    }
    else
    {
        path = dirName;
    }




    FILE *fp = fopen(path, "r");
    char *line = NULL;
	size_t len = 246;
	ssize_t x;
    line = (char*)malloc(len*sizeof(char));
    //getline(&line, &len, fp);
    while((x = getline(&line, &len, fp)) != -1)
    {
        int counter = 0;
        char hasFinished = '0';
        char startedCounter = '0';
        int start = 0;
        int end = 0;


        while(hasFinished == '0')
        {
            if(isalpha(line[counter]))
            {
                if(startedCounter == '0')
                {
                    start = counter;
                    startedCounter = '1';
                }
            }
            else
            {
                if(startedCounter == '1')
                {
                    end = counter;
                    startedCounter = '0';

                    int loopCount = start;
                    if(end>start)
                    {
                        char *temp = (char*)malloc(sizeof(char)*(end-start)+1);
                        int tempCount = 0;
                        for(loopCount = start; loopCount< end; loopCount++)
                        {
                            temp[tempCount] = line[loopCount];
                            tempCount++;
                        }
                        temp[end-start+1] = '\0';

                        inserter(temp, path);
                    }
                }
            }
            if(line[counter] == '\0')
                hasFinished = '1';
            counter++;
        }
    }
    fclose(fp);
    //free(path);
}


int checkIfDir(char *name)
{
    int value = 0;
    int ct = 0;
    for(ct = 0; ct< strlen(name); ct++)
    {
        if(name[ct] == '.')
            value = 1;
    }
    return value;
}

void traverse(const char *filePath)
{

    DIR *directory;
    struct dirent *dir;
    directory = opendir(filePath);
    if(directory != NULL)
    {
        while((dir = readdir(directory)) != NULL)
        {
            if(dir->d_name[0] != '.')
            {
                struct stat path_stat;
                stat(dir->d_name, &path_stat);
                //printf("%d\t %s\n",S_ISREG(path_stat.st_mode), dir->d_name);
                if(S_ISREG(path_stat.st_mode) == 0)
                {
                    int checkDir = checkIfDir(dir->d_name);
                    if(checkDir == 0)
                    {
                        int sizeOfNewPath = strlen(filePath)+1+strlen(dir->d_name)+1;
                        char *newPath = (char*)malloc(sizeof(char)*sizeOfNewPath);
                        strcpy(newPath, filePath);
                        newPath[strlen(filePath)] = '/';

                        int k = 0;
                        int ct = 0;
                        for(k = strlen(filePath)+1; k<sizeOfNewPath-1; k++)
                        {
                            newPath[k] = dir->d_name[ct];
                            ct++;
                        }
                        newPath[k] = '\0';




                        //printf("%s\n", newPath);
                        traverse(newPath);

                        //free(newPath);
                    }
                    else
                    {
                        if(checkIfValid(dir->d_name) == 1)
                        {
                            txtReader(filePath, dir->d_name);
                            //printf("TXT file %s\n", dir->d_name);
                        }
                    }
                }
                else
                {
                    //first level check if file is txt

                    if(checkIfValid(dir->d_name) == 1)
                    {
                        txtReader(filePath, dir->d_name);
                        //printf("TXT file %s\n", dir->d_name);
                    }
                }
            }

        }
        closedir(directory);
    }
    else
    {

        txtReader(filePath, (char*) filePath);
    }


}




/////// sorting for  occurrence

fileNode *divideList(fileNode *fn)
{
    //even skips 2 nodes at a time, normal is odd nodes at a time
    fileNode *even = fn;
    fileNode *odd = fn;
    fileNode *prev = NULL;

    while (even != NULL && even->nextNode != NULL)
    {
       even = even->nextNode->nextNode;
       prev = odd;
       odd = odd->nextNode;
    }

    if (prev != NULL)
    {
       prev->nextNode = NULL;
    }
    return odd;
}

fileNode* merge(fileNode *list1, fileNode *list2)
{

    fileNode *newList = (fileNode*)malloc(sizeof(fileNode));
    //gets the remaining portion after 1 list is depleted
    fileNode *end = newList;

    while ( (list1 != NULL) && (list2 != NULL) )
    {
        //sorts the max so that occurrence> will proceed
        fileNode **max = NULL;
        if(list1->occurrence > list2->occurrence)
            max = &list1;
        else
            max = &list2;

        fileNode *next = (*max)->nextNode;
        end->nextNode = *max;
        end = end->nextNode;
        *max = next;

        //adds next to the end not losing its nextNode
    }

    if(list1!= NULL)
        end->nextNode = list1;
    else
        end->nextNode = list2;
    return newList->nextNode;
}

fileNode *sort(fileNode *head)
{
    fileNode *list1 = head;
    if ( (list1 == NULL) || (list1->nextNode == NULL) )
    {
       return list1;
    }
    //divide 2
    fileNode *list2 = divideList(list1);

    //and divide 2 and merge;
    head = merge( sort(list1), sort(list2) );
    return head;
}

//merge sort for fileNames

nameList *helpList(nameList *nm)
{
    //even skips 2 nodes at a time, normal is odd nodes at a time
    nameList *even = nm;
    nameList *odd = nm;
    nameList *prev = NULL;

    while (even != NULL && even->next != NULL)
    {
       even = even->next->next;
       prev = odd;
       odd = odd->next;
    }

    if (prev != NULL)
    {
       prev->next = NULL;
    }
    return odd;
}


nameList* herge(nameList * list1, nameList* list2)
{
    nameList *newList = (nameList*)malloc(sizeof(nameList));
    //gets the remaining portion after 1 list is depleted
    nameList *end = newList;

    while ( (list1 != NULL) && (list2 != NULL) )
    {
        //sorts the max so that occurrence> will proceed
        nameList **max = NULL;
        if(strcmp(list1->value, list2->value)<0)
            max = &list1;
        else
            max = &list2;

        nameList *next = (*max)->next;
        end->next = *max;
        end = end->next;
        *max = next;

        //adds next to the end not losing its nextNode
    }

    if(list1!= NULL)
        end->next = list1;
    else
        end->next = list2;
    return newList->next;
}

nameList* helpNames(nameList *nt)
{
    nameList *list1 = nt;
    if ( (list1 == NULL) || (list1->next == NULL) )
    {
       return list1;
    }
    //divide 2
    nameList *list2 = helpList(list1);

    //and divide 2 and merge;
    nt = herge( helpNames(list1), helpNames(list2) );
    return nt;
}

fileNode *swapNodes(fileNode *head, nameList* list, int ocVal)
{
    fileNode* ptr = head;
    while(ptr!= NULL && list!= NULL)
    {
        if(ocVal == ptr->occurrence)
        {
            ptr->fileName = list->value;
            list = list->next;
        }
        ptr = ptr->nextNode;
    }
    return head;
}

fileNode* sortNames(fileNode *head)
{
    if(head->nextNode == NULL)
    {
        return head;
    }
    fileNode *ptr = head;
    int value = 0;
    fileNode *nxt = ptr->nextNode;
    int count = 0;
    nameList *nl = (nameList*)malloc(sizeof(nameList));
    nameList *pointer = NULL;
    while(ptr != NULL && ptr->nextNode != NULL)
    {
        //printf("%s %s %d\n", ptr->fileName, nxt->fileName, count);
        if(nxt->occurrence == ptr->occurrence)
        {
            if(count == 0)
            {
                value = nxt->occurrence;
                nameList *head = (nameList*)malloc(sizeof(nameList));
                head->value = ptr->fileName;
                head->next = NULL;
                nl = head;
                pointer = nl;
            }
            count++;

            nameList *temp = (nameList*)malloc(sizeof(nameList));
            temp->value = nxt->fileName;
            temp->next = NULL;
            pointer->next = temp;
            nxt = nxt->nextNode;
            pointer = pointer->next;


            //printf("%d\n", count);
        }
        else
        {
            if(count == 0)
            {
                value = 0;
                ptr = ptr->nextNode;
                nxt = ptr->nextNode;
            }
            else
            {
                nl = helpNames(nl);
                head = swapNodes(head, nl, value);
                //nameList *np = nl;

                value = 0;
                count = 0;
                nl = NULL;
            }
        }

        if(nxt == NULL)
            break;
    }

    if(nl!= NULL)
    {
        nl = helpNames(nl);
        nameList *np = nl;
        head = swapNodes(head, nl, value);
        // while(np!= NULL)
        // {
        //     printf("%s\n", np->value);
        //     np = np->next;
        // }
    }
    return head;

}


/////////

//xml

void xmlPrint(node* head, char* xmlName)
{
    FILE *fp;

    fp = fopen(xmlName, "w");
    fprintf(fp, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<fileIndex>\n");
    if(head == NULL)
    {
        fprintf(fp,"<word>\n</word>");
    }
    else
    {
        node* prev = NULL;
        node* ptr = head;
        while(ptr != NULL)
        {
            fprintf(fp,"\t<word text = \"%s\">\n", ptr->value);
            fileNode* fn = ptr->files;
            while(fn != NULL)
            {
                fprintf(fp,"\t\t<file name = \"%s\">%d</file>\n", fn->fileName,fn->occurrence);
                fn = fn->nextNode;
            }
            
            prev = ptr;
            ptr = ptr->next;

            fprintf(fp,"\t</word>\n");
        }
    }

    fprintf(fp, "</fileIndex>");
    fclose(fp);
}


int main(int argc, char ** argv)
{
    if(argc< 3)
        return 0;

    char *reverseName = argv[1];
    char *startPos = argv[2];
    //printf("%s", startPos);
    //traverse(startPos);
    traverse(startPos);

    //bubbleSort();
    node* ptr = head;

    xmlPrint(head, reverseName);
    // while(ptr!= NULL)
    // {
    //     printf("%s %d\n", ptr->value,ptr->fileSize);
    //     fileNode *fn = ptr->files;
    //     fn = sort(fn);
    //     fn = sortNames(fn);
    //     do
    //     {
    //         printf("\t\t%s -> %d\n", fn->fileName, fn->occurrence);
    //         fn = fn->nextNode;
    //     }while(fn != NULL);
    //     ptr = ptr->next;
    // }



}
