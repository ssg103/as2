#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>


typedef struct fileNode
{
    char *fileName;
    int occurrence;
    struct fileNode* nextNode;

}fileNode;

typedef struct node
{
    char *value;
    struct fileNode *files;
    struct node *next;
    int fileSize;

}node;

typedef struct nameList
{
    char* value;
    struct nameList* next;
}nameList;
